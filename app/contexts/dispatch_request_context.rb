class DispatchRequestContext
  include Shop2::Objectifier

  def self.call request, options={}
    new(request).call
  end

  def initialize request, options={}
    @negate = request[:negate]
    @action = request[:action]
    @actor = request[:actor]
    @object = request[:object] || objectify(request[:object_string])
    @target = request[:target] || objectify(request[:target_string])
  end

  def call
    context.new(actor: @actor, role: role, action: @action, object: @object, target: @target).call
  end

private

  def context
    "#{@action.capitalize}#{@object.class.name}".constantize
  end

  def role
    case @action
    when "follow"
      Follower
    when "love"
      Lover
    when "collect"
      Collector
    end
  end
end
