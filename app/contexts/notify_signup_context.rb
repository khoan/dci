class NotifySignupContext
  def self.call options
    new(options).call
  end

  def initialize options
    @freshman = options[:user].as(Freshman)
  end

  def call
    @freshman.receiving_induction_email
    @freshman.announcing_induction_to_friends
  end
end
