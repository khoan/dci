class SignupAndDispatchRequestContext
  def self.call user_attributes, request, options={} 
    new(user_attributes, request, options).call
  end

  def initialize user_attributes, request, options={}
    @user_attributes = user_attributes
    @request = request
    @options = options
  end

  def call
    guest = SignupContext.call(user_attributes, options)

    DispatchRequestContext.call(request.merge(actor: guest), options)
  end
end
