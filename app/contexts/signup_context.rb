class SignupContext
  def self.call(user_attributes)
    new(user_attributes).call
  end

  def initialize(user_attributes)
    @person = User.new(user_attributes)
    @visitor = person.as(Visitor)
  end

  def call
    guest = @visitor.signup

    NotifySignupContext.call(freshman: guest)

    guest
  end
end
