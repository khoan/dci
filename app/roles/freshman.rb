class Freshman < SimpleDelegator
  def receiving_induction_email
  end

  def announcing_induction_to_friends
  end
end
