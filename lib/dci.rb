module DCI
  module Data
    def as(role_class)
      role_class.new self
    end
  end
end
