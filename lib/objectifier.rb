module Shop2
  module Objectifier
    def objectify string
      object_class, object_id = String(string).split(object_separator)
      object_class.constantize.find object_id
    end

  private

    def object_separator
      ":"
    end
  end
end
